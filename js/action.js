//----new lesson--------
//http://www.test.ru/api/users  - получить список пользователей
//http://www.test.ru/api/login -

//GET, POST, PATCH, DELETE, PUT

//http://www.test.ru/api/users?result=10
//http://www.test.ru/api/users?from=10&to=30&result=5

// Promise

const container =document.querySelector('.container');
const inputName = document.querySelector('.add-name');
const addBtn = document.querySelector('.add-btn');
const fileBtn = document.querySelector('.input-file');
const imgFile = document.querySelector('.preview');

let loadedImage;

addBtn.addEventListener('click', addUser);
fileBtn.addEventListener('change', loadImage);


let myUserList = [];

const xhr = new XMLHttpRequest();
xhr.open('GET', 'https://randomuser.me/api?results=10');

//xhr.addEventListner('readystatechange', check)
xhr.onreadystatechange = function() {
    if (xhr.readyState === 4) {
        if (xhr.status === 200) {
            const data = JSON.parse(xhr.responseText);
            const userList = data.results;
            myUserList = convertUserData(userList);
            drawUsers();
        }
    }
}

xhr.send();

function drawUsers() {
    container.innerHTML = '';
    myUserList.forEach(item => {
        const userTag = getUserCard(item);
        container.appendChild(userTag);
    });
}

function convertUserData(users) {
    return users.map(item => {
        return {
            id: item.login.uuid,
            name: item.name.first + ' ' + item.name.first,
            avatar: item.picture.large,
        }
    });
}

function getUserCard(user) {
    const card = document.createElement('div');
    card.classList.add('card');

    const avatar = document.createElement('img');
    avatar.classList.add('avatar');
    avatar.src = user.avatar;

    const info = document.createElement('div');
    info.classList.add('info');

    const userName = document.createElement('span');
    userName.classList.add('user-name');
    userName.innerText = user.name;

    const deleteBtn = document.createElement('div');
    deleteBtn.classList.add('delete');
    deleteBtn.innerText = 'x';

    const deleteHandler = () => deleteItem(user.id);
    deleteBtn.addEventListener('click', deleteHandler)


    info.appendChild(userName);
    card.appendChild(deleteBtn);
    card.appendChild(avatar);
    card.appendChild(info);
    
    return card;
}

function deleteItem(id) {
    const user = myUserList.find(item => item.id === id);
    const index = myUserList.indexOf(user);
    myUserList.splice(index, 1);
    drawUsers();
}

// --------- new lesson--------------

function addUser() {
    const userName = inputName.value;
    const userObj = getUserObj(userName);
    myUserList.push(userObj);
    inputName.value = '';
    fileBtn.value = '';
    imgFile.src = './image/stub.png';
    loadedImage = null;
    drawUsers();
    scrollToBottom();
}

function getRandomId() {
    return Math.floor(Math.random() * (1001));
}

function getUserObj(name) {
    return {
        id: getRandomId(),
        name,
        avatar: loadedImage ? loadedImage : './image/stub.png',
    };
}

function scrollToBottom() {
        const cards = document.querySelectorAll('.card');
        const lastCard = cards[cards.length - 1];
        const lastCardCoords = lastCard.getBoundingClientRect();// возвращает кардинаты элеметна

        container.scrollTo({
            top: lastCardCoords.y,
            behavior: 'smooth',
        });
}

function loadImage(event) {
    const file = event.target.files[0];
    if (file && file.type.includes('image')) {
        const fileReader = new FileReader();

        fileReader.onloadend = (data) => {
            imgFile.src = data.target.result; //base64
            loadedImage = data.target.result;
        }

        fileReader.readAsDataURL(file);
    }    
}

