const name = document.querySelector('#name');
const age =document.querySelector('#age');
const addBtn = document.querySelector('.add');
const clearBtn = document.querySelector('.clear'); 
const deleteBtn = document.querySelector('.delete');

addBtn.addEventListener('click', addHandler);
clearBtn.addEventListener('click', clearUserList);
deleteBtn.addEventListener('click', deleteLastItem);

console.log(getUserList());

function getUserObj() {
    const userName = name.value;
    const userAge = age.value;
    const userObj = {
        name: userName,
        age: userAge,    
    };
    return userObj;
}

function clearFilds() {
    name.value = '';
    age.value = '';
}

function addHandler() {
    const userObj = getUserObj();
    clearFilds();
    const userList = getUserList();
    userList.push(userObj);
    saveUserList(userList);
}

function getUserList() {
    const lsData = localStorage.getItem('userlist');
    if (lsData) {
        const parsData = JSON.parse(lsData);
        return parsData;
    }
    return [];
}

function saveUserList(users) {
    const stringifyList = JSON.stringify(users);
    localStorage.setItem('userlist', stringifyList);
}

function clearUserList() {
    localStorage.removeItem('userlist');
}

function deleteLastItem() {
    const userList = getUserList();
    if (userList.length > 0) {
        userList.pop();
        saveUserList(userList);
    }
}
